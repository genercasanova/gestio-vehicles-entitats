
<?php
// We need to use sessions, so you should always start sessions using the below code.
require '../calendari/con_db.php';

session_start();
// If the user is not logged in redirect to the login page...
if (!isset($_SESSION['sess_username'])) {
	header('Location: ../Login/index.php');
	exit();
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
                <link rel="icon" href="../imatges/favicon.ico" type="image/ico">
		<title>Home Page</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
                <link rel="stylesheet" href="./css/all.css">
                <script src="jquery.min.js"></script>
                <script src="../gestio_usuaris/jquery.min.js"></script>
        <link rel="stylesheet" href="../login/css/bootstrap.min.css">

	</head>
	<body class="loggedin">
		<nav class="navtop">
			<div>
                            <h1><img src="../imatges/header masies.png"></h1>
                                <button class="btn btn-warning" name="tancar" onclick="location.href='../Login/logout.php'" id="tancar" style="float:right;margin:10px;background-color: #8F9D32">Tancar sessió</button>
                               
			</div>
		</nav>
            <p>
		<div class="content">
                   
                    <h2>&nbsp Espai administració</h2>
			<p>&nbsp Benvingut/da de nou <?=$_SESSION['sess_username']?>!</p>
                      
                        
                            <a href="../calendari/cal-admin.php"> <img src="../imatges/gestio_calendari.png"style="width:120px;height:100px;" hspace="50"></a>
                            
                            <a   href="../gestio_usuaris/gest-usuaris.php">  <img src="../imatges/gestio_usuaris.png"style="width:120px;height:120px;" hspace="50"> </a>
      
                            <a href="../gestio_recursos/gest-recursos.php">  <img src="../imatges/gestio_recursos.png" style="width:120px;height:110px;"hspace="50"></a>
                        
                            <a href="../gestio_tipus_recurs/gest-tip-rec.php"> <img src="../imatges/tipus_recurs.png" style="width:120px;height:100px;" hspace="50"></a>
                       
		
           
	</body>
</html>
