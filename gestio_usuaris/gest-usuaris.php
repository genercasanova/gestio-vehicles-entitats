
<?php
// We need to use sessions, so you should always start sessions using the below code.
require '../calendari/con_db.php';

session_start();
// If the user is not logged in redirect to the login page...
if (!isset($_SESSION['sess_username'])) {
	header('Location: ../Login/index.php');
	exit();
}
?>
<!Doctype html>
<html>
<head>
        <link rel="icon" href="../imatges/favicon.ico" type="image/ico">
	<title>Editar usuaris i usuaries</title>
        <script src="jquery.min.js"></script>
        <link rel="stylesheet" href="../login/css/bootstrap.min.css">
</head>
<body>
	<div class="container" style="text-align:center;">
		<h1>Gestió d'usuaris i usuaries</h1>
		<button class="btn btn-warning" id="add_data" style="float:right;margin:10px;background-color: #8F9D32">Afegir</button>
                <button class="btn btn-warning"  onclick="location.href='../espaiAdmin/admin.php'"  name="tornar"  id="tornar"  style="float:left;margin:10px;background-color: #8F9D32">Tornar</button>
		<table class="table table-bordered" id="show_data">
			<tr>
                                <th>Id</th>
                                <th>nom</th>
                                <th>login</th>
                                <th>password</th>
				<th>admin</th>
				<th>Menu</th>
			</tr>
			<tr id="add_data_field" style="display:none;">
                            <td></td>
                                <td><input type="text" placeholder="nom" id="nom" class="form-control"></td>
				<td><input type="email" placeholder="login" id="login" class="form-control" /></td>
				<td><input type="text" placeholder="contrasenya" id="password" class="form-control" /></td>
				<td><input type="email" placeholder="admin: 1=SI / 0=NO" id="admin" class="form-control" /></td>
				<td><button class="btn" id="add">Afegir</button></td>
			</tr>
			<?php
			// creating database connection
			// here user_details is a database
			
			$sel = "select * from users order by id desc";
			$query = $mysqli->query($sel);
			if(mysqli_num_rows($query) > 0)
			{
				while($data = mysqli_fetch_array($query))
				{
					echo '
						<tr>
                                                        <td>'.$data['id'].'</td>
                                                        <td>'.$data['nom'].'</td>
                                                        <td>'.$data['login'].'</td>
							<td>'.$data['password'].'</td>
							<td>'.$data['admin'].'</td>
							<td>
								<button class="btn edit" id='.$data['id'].'>Editar</button>
								<button class="btn delete" id='.$data['id'].'>Eliminar</button>
							</td>
						</tr>
					';
				}
			}
			else
			{
				echo '
						<tr>
							<td>No  Data found</td>
							<td>No  Data found</td>
							<td>No  Data found</td>
                                                        <td>No  Data found</td>
						</tr>
					';
			}
			?>
		</table>
	</div>
	<script type="text/javascript">
	// add_data
    $(document).on('click','#add_data',function (){
         $('#add_data_field').show();
    });
	// add data in database
    $(document).on('click','#add',function (){
        var lg = $('#nom').val();
        var nm = $('#login').val()
        var ps = $('#password').val();
        var ad = $('#admin').val();
		$.ajax({
			url:"add_data.php",
			method:"post",
			data:{lg:lg,nm:nm,ps:ps,ad:ad},
			success:function(data){
				$('#show_data').html(data);
			}
		});
    });
	// Show edit data
	$(document).on('click','.edit',function (){
        var id = $(this).attr('id');
		var edit = 'show_edit_data';
		$.ajax({
			url:"edit_data.php",
			method:"post",
			data:{id:id,edit:edit},
			success:function(data){
				$('#show_data').html(data);
			}
		});
    });
	// edit data
	$(document).on('click','.update',function (){
        var login = $('#new_login').val();
        var nom = $('#new_nom').val();
        var password = $('#new_password').val();
        var admin = $('#new_admin').val();
		var update_id = $(this).attr('id');
		var edit = 'edit_data';
		$.ajax({
			url:"edit_data.php",
			method:"post",
			data:{nom:nom,login:login,password:password,admin:admin,edit:edit,update_id:update_id},
			success:function(data){
				$('#show_data').html(data);
			}
		});
    });
	// delete data
	$(document).on('click','.delete',function (){
        var del_id = $(this).attr('id');
		$.ajax({
			url:"delete_data.php",
			method:"post",
			data:{del_id:del_id},
			success:function(data){
				$('#show_data').html(data);
			}
		});
    });
	</script>
</body>
</html>