
<?php
// We need to use sessions, so you should always start sessions using the below code.
require '../calendari/con_db.php';

session_start();
// If the user is not logged in redirect to the login page...
if (!isset($_SESSION['sess_username'])) {
	header('Location: ../Login/index.php');
	exit();
}
?>
<?php 
require_once '../calendari/con_db.php';

	
	$query = "SELECT id, nom from tipologia_recursos";
	$resultado=$mysqli->query($query);
?>



<!Doctype html>
<html>
<head>
        <link rel="icon" href="../imatges/favicon.ico" type="image/ico">
	<title>Gestió de recursos -AJMV</title>
	<script src="jquery.min.js"></script>
        <link rel="stylesheet" href="../login/css/bootstrap.min.css">
        

</head>

	<div class="container" style="text-align:center;">
		<h1>Gestió de recursos</h1>
                <div>per afegir un recurs selecciona primer el tipus </div><br/>
                 <div style="text-align:center;">
                <select onchange='add_data.disabled=false;mostrarValor(this.options[this.selectedIndex].innerHTML);'  name='tipus' id='tipus'>
                    
                    <option>seleccionar...</option>
                        <?php while($row = $resultado->fetch_assoc()) { ?>
					<option value="<?php echo $row['id']; ?>"><?php echo $row['nom']; ?></option>
				<?php } ?>
                </select>
                 </div>
		<button class="btn btn-warning" name="add_data" id="add_data" disabled='true' style="float:right;margin:10px;background-color: #8F9D32">Afegir </button>
                <button class="btn btn-warning"  onclick="location.href='../espaiAdmin/admin.php'"  name="tornar"  id="tornar"  style="float:left;margin:10px;background-color: #8F9D32">Tornar</button>
                
		<table class="table table-bordered" id="show_data">
			<tr>
				<th>ID</th>
				<th>Nom</th>
				<th>Descripcio</th>
                                <th>Tipus de recurs</th>
                                
			</tr>
			<tr id="add_data_field" style="display:none;">
                            <td></td>
				<td><input type="text" placeholder="nom" id="nom" class="form-control" /></td>
				<td><input type="text" placeholder="descripcio" id="descripcio" class="form-control" /></td>
                                <td><input type="text" placeholder="tipus" disabled='true' id="tipsa" class="form-control" /></td>
                       
                                 
                              
				<td><button class="btn" id="add">Afegir</button></td>
                                
                        <script>
                            var mostrarValor = function(x){
        document.getElementById('tipsa').value=x;
        }
        </script>
			<?php
			// creating database connection
			// here user_details is a database
			
			$sel = "select tr.id, tr.nom as name, tr.descripcio, ttr.nom from recursos tr inner join tipologia_recursos ttr where tr.id_tipus_recurs=ttr.id order by id desc";
			$query = $mysqli->query($sel);
			if(mysqli_num_rows($query) > 0)
			{
				while($data = mysqli_fetch_array($query))
				{
					echo '
						<tr>
                                                        
							<td>'.$data['id'].'</td>
							<td>'.$data['name'].'</td>
                                                        <td>'.$data['descripcio'].'</td>
                                                        <td>'.$data['nom'].'</td>
							<td>
								<button class="btn edit" id='.$data['id'].'>Editar</button>
								<button class="btn delete" id='.$data['id'].'>Eliminar</button>
							</td>
						</tr>
					';
				}
			}
			else
			{
				echo '
						<tr>
							<td>No  Data found</td>
							<td>No  Data found</td>
							<td>No  Data found</td>
                                                        <td>No  Data found</td>
							<td>No  Data found</td>
						</tr>
					';
			}
			?>
		</table>
	</div>
	<script type="text/javascript">
	// add_data
    $(document).on('click','#add_data',function (){
         $('#add_data_field').show();
    });
	// add data in database
    $(document).on('click','#add',function (){
        var nm= $('#nom').val();
        var ds = $('#descripcio').val();
        var tr = $('#tipsa').val();
      
		$.ajax({
			url:"add_data.php",
			method:"post",
			data:{nm:nm,ds:ds,tr:tr},
			success:function(data){
				$('#show_data').html(data);
                                select_box = document.getElementById("tipus");
                        select_box.selectedIndex = 0;
			}
		});
    });
	// Show edit data
	$(document).on('click','.edit',function (){
        var id = $(this).attr('id');
		var edit = 'show_edit_data';
		$.ajax({
			url:"edit_data.php",
			method:"post",
			data:{id:id,edit:edit},
			success:function(data){
				$('#show_data').html(data);
			}
		});
    });
	// edit data
	$(document).on('click','.update',function (){
        var nom = $('#new_nom').val();
        var descripcio = $('#new_descripcio').val();
        var tipus_recurs = $('#new_tipus_recurs').val();
     
		var update_id = $(this).attr('id');
		var edit = 'edit_data';
		$.ajax({
			url:"edit_data.php",
			method:"post",
			data:{nom:nom,descripcio:descripcio,password:tipus_recurs,edit:edit,update_id:update_id},
			success:function(data){
				$('#show_data').html(data);
			}
		});
    });
	// delete data
	$(document).on('click','.delete',function (){
        var del_id = $(this).attr('id');
		$.ajax({
			url:"delete_data.php",
			method:"post",
			data:{del_id:del_id},
			success:function(data){
				$('#show_data').html(data);
			}
		});
    });
	</script>
</body>
</html>