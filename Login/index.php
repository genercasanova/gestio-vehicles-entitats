<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../imatges/favicon.ico" type="image/ico">
    <title>Gestió vehicles i recursos</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  
  <body>
    <div class="container">
      <div class="info">
         <h2 style="background-color: #8F9D32" class="bg-primary">Gestió de vehicles i recursos.</h2>
         
       
          <div class="col-md-6 col-md-offset-3">
                    <h4></span>Introdueix nom d'usuari i contrasenya per entrar<span class="glyphicon glyphicon-user"></h4><br/>
                            <div class="block-margin-top">
                              <?php 

                                $errors = array(
                                    1=>"Nom i/o contrasenya incorrecta.",
                                    2=>"Si us plau, identifica't per entrar."
                                  );

                                $error_id = isset($_GET['err']) ? (int)$_GET['err'] : 0;

                                if ($error_id == 1) {
                                        echo '<p class="text-danger">'.$errors[$error_id].'</p>';
                                    }elseif ($error_id == 2) {
                                        echo '<p class="text-danger">'.$errors[$error_id].'</p>';
                                    }
                               ?>  

                              <form action="authenticate.php" method="POST" class="form-signin col-md-8 col-md-offset-2" role="form">  
                                  <input type="text" name="username"  class="form-control" placeholder="Nom usuari" required autofocus><br/>
                                  <input type="password" name="password" class="form-control" placeholder="Contrasenya" required><br/>
                                  <button class="btn btn-lg btn-primary btn-block"style="background-color: #8F9D32" type="submit">Entrar</button>
                                  <p>
                                      <br>
                                      <br>
                                      <br>
                                  </p>
                                  
                                      
                                  <img src="../imatges/header masies.png">
                                  </form>
                              
                               

                           
            </div>

      </div>
      
     
    </div>
  

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../calendari/js/jquery-3.2.1.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    
  </body>
</html>